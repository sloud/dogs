
#pragma once

#include <variant>

#include "../core/Functor.hpp"

namespace dogs {

/**
 * @brief
 *
 * @tparam A
 */
template <typename A>
struct Some {
  const A inner;
  Some(const A a) : inner{a} {};
};

/**
 * @brief
 *
 * @tparam A
 */
template <typename A>
struct None {};

template <typename A>
struct Option;

template <typename A>
inline auto some(const A a) -> Option<A>;

template <typename A>
inline auto none() -> None<A>;

/**
 * @brief
 *
 * @tparam
 * @tparam A
 * @tparam Func
 * @param fa
 * @param f
 * @return Option<std::result_of_t<Func(A)>>
 */
template <>
template <typename A, typename Func>
auto Functor<Option>::map(const Option<A> &fa, Func &&f)
    -> Option<std::result_of_t<Func(A)>> {
  if (fa.isSome())
    return some(f(std::get<Some<A>>(fa).inner));
  else
    return none<std::result_of_t<Func(A)>>();
}

/**
 * @brief
 *
 * @tparam A
 */
template <typename A>
struct Option : std::variant<Some<A>, None<A>> {
  using std::variant<Some<A>, None<A>>::variant;

  inline constexpr bool isSome() const { return this->index() == 0; }
  inline constexpr bool isNone() const { return this->index() == 1; }
};

/**
 * @brief
 *
 * @tparam A
 * @param a
 * @return Option<A>
 */
template <typename A>
inline auto some(const A a) -> Option<A> {
  return Some<A>{a};
}

/**
 * @brief
 *
 * @tparam A
 * @return None<A>
 */
template <typename A>
inline auto none() -> None<A> {
  return None<A>{};
}

}  // namespace dogs
