#pragma once

#include <ostream>

namespace dogs {

// return type for void functions
enum class Unit { _ };

inline std::ostream &operator<<(std::ostream &os, const Unit &unit) {
  os << "Unit";
  return os;
}

namespace alias {

const auto _ = Unit::_;

}  // namespace alias

}  // namespace dogs
