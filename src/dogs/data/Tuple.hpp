/**
 * @file Tuple.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-03-22
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once

#include <ostream>
#include <tuple>

namespace dogs {

// a tuple type
template <typename... As>
auto tuple(As&&... args) -> std::tuple<As...> {
  return std::make_tuple(args...);
}

}  // namespace dogs
