/**
 * @brief
 *
 */
#pragma once

#include "../core/Functor.hpp"

namespace dogs {

template <typename A>
using Id = A;

template <>
template <typename A, typename Func>
auto Functor<Id>::map(const Id<A> &fa, Func &&f)
    -> Id<std::result_of_t<Func(A)>> {
  return f(fa);
}

}  // namespace dogs
