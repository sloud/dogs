#ifndef __DOGS_FN__
#define __DOGS_FN__

#include <functional>
#include <ostream>

namespace dogs {

template <class A> using fn = std::function<A>;

template <class R, class... A> using fp = R (*)(A...);

template <class A> std::ostream &operator<<(std::ostream &os, fn<A> f) {
  os << "function";
  return os;
}

// helper type for the visitor
template <class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template <class... Ts> overloaded(Ts...)->overloaded<Ts...>;

} // namespace dogs

#endif // __DOGS_FN__
