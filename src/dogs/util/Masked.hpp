/**
 * @brief
 *
 */
#ifndef __DOGS_MASKED__
#define __DOGS_MASKED__

namespace dogs {

/**
 * @brief
 *
 * @tparam A
 */
template <class A>
struct Masked {
  /**
   * @brief
   *
   * @return A
   */
  virtual A self() const = 0;
};

}  // namespace dogs

#endif  // __DOGS_MASKED__