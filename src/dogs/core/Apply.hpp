#ifndef __DOGS_APPLY__
#define __DOGS_APPLY__

#include <ostream>
#include "dogs/core/Functor.hpp"
#include "dogs/util/fn.hpp"

namespace dogs {

  template <template <class> class F>
  struct Apply : Functor<F> {
    template <class A, class B>
    static F<B> const ap(const F<A> &fa, const F<fn<B(A)>> &ff) {
      return fa.template ap<B>(ff);
    }

    template <class A>
    struct Ops : Functor<F>::template Ops<A> {
      /**
       * template <class B>
       * F<B> map(fn<B(A)> f) const;
       */

      /**
       * template <class B>
       * F<B> ap(const F<fn<B(A)>> &ff) const;
       */
    };
  };
}  // namespace dogs

#endif  // __DOGS_APPLY__
