#include "dogs/core/Apply.hpp"
#include "fn.hpp"

namespace dogs {

  template <template <class> class F>
  struct Applicative : Apply<F> {
    template <class A, class B>
    static F<B> ap(const F<A> &fa, const F<fn<B(A)>> &ff);

    template <class A>
    static F<A> pure(const A &a);

    template <class A, class B>
    static F<B> map(const F<A> &fa, fn<B(A)> f) {
      return ap(fa, pure(f));
    }

    template <class A>
    struct Ops : Apply<F>::template Ops<A> {
      static auto box(const F<A> &fa) { return Ops<A>{fa}; }

      template <class B>
      auto ap(const F<fn<B(A)>> &ff) {
        return Ops<B>::box(Applicative<F>::ap<A, B>(this->unbox(), ff));
      }

      auto pure(const A &a) { return Ops<A>::box(Applicative<F>::pure<A>(a)); }

      template <class B>
      auto map(fn<B(A)> f) {
        return Ops<B>::box(Applicative<F>::map<A, B>(this->unbox(), f));
      }
    };
  };

}  // namespace dogs
