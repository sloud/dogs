/**
 * @file Functor.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-03-22
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once

#include <utility>

namespace dogs {

/**
 * @brief
 *
 * @tparam F
 */
template <template <typename> typename F>
struct Functor {
  /**
   * @brief core functor operation.
   *
   * @tparam A
   * @tparam Func
   * @param fa
   * @param f
   * @return F<std::result_of_t<Func(A)>>
   */
  template <typename A, typename Func>
  static auto map(const F<A> &fa, Func &&f) -> F<std::result_of_t<Func(A)>>;

  template <typename A, typename Func>
  static auto lift(Func &&f) {
    return [&](const F<A> &fa) { return map(fa, f); };
  }

  template <typename A>
  struct Box;

  /**
   * @brief
   *
   * @tparam A
   * @param fa
   * @return Box<A>
   */
  template <typename A>
  inline static auto box(const F<A> &fa) -> Box<A> {
    return Box<A>{fa};
  }

  /**
   * @brief
   *
   * @tparam A
   */
  template <typename A>
  struct Box {
   private:
    const F<A> inner;

   public:
    Box(const F<A> fa) : inner{fa} {};
    auto unbox() -> F<A> { return inner; }

    /**
     * @brief
     *
     * @tparam Func
     * @param f
     * @return Box<std::result_of_t<Func(A)>>
     */
    template <typename Func>
    inline auto map(Func &&f) -> Box<std::result_of_t<Func(A)>> {
      return box(Functor<F>::map(inner, f));
    }
  };
};

}  // namespace dogs
