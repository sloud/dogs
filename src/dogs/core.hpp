/**
 * @file all.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-03-22
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once

#include <dogs/core/Functor.hpp>
#include <dogs/data/Id.hpp>
#include <dogs/data/Option.hpp>
#include <dogs/data/Tuple.hpp>
#include <dogs/data/Unit.hpp>

namespace dogs {

using namespace dogs::alias;

}  // namespace dogs
