#include <dogs/core.hpp>
#include <iostream>
#include <string>

using namespace dogs;

template <typename A>
auto print(const A a) -> Unit {
  std::cout << std::to_string(a) << std::endl;
  return _;
}

int main(int argc, char const *argv[]) {
  auto s = some(1);

  Functor<Option>::map(s, print<int>);

  Functor<Option>::map(s, [](auto i) {
    std::cout << "inner = " << i << std::endl;
    return _;
  });

  auto box = Functor<Option>::box(s);

  box.map(print<int>);
  box.map([](auto i) {
    std::cout << "inner = " << i << std::endl;
    return _;
  });

  box.map([](auto i) { return i + 10; })
      .map([](auto i) { return i * 10; })
      .map(print<int>)
      .map([](auto u) { return 6; })
      .map(print<int>);

  /* code */
  return 0;
}
