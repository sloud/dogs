:q

tup: FORCE
	tup

init: FORCE
	tup init

variants: FORCE
	tup variant config/dev.config

docs: FORCE
	cd docs && ./build

serve-docs: FORCE
	cd docs/html && python -m http.server

clean: FORCE
	rm -rf build-*
	rm -rf docs/html docs/latex

FORCE:

