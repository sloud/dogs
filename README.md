# Dogs

A `C++` port of the `scala` library [cats][2]. Check it out on [gitlab][1] or clone it with

```
git clone git@gitlab.com:sloud/dogs.git
```

[1]: https://gitlab.com/sloud/dogs
[2]: https://typelevel.org/cats/
